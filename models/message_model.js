var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var message_schema = new Schema({
	sent_by: { type: String, required: true},
	message: { type: String, required: true},
	uid: { type: String, required: true}
});

module.exports = mongoose.model('message', message_schema);