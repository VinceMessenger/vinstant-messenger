var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var room_schema = new Schema({
	name: { type: String, required: true },
	moderators: { type: Array, required: true },
	users: {type: Array, required: true},
	messages: {type: Array, required: true}
});

room_schema.statics.getMessageArray = function(room, cb) {
	this.model('room').findOne({name: room}, function(err, room) {
		if (err)
			throw err;
		
		console.log(room);
		cb(room);  // return chat room message array
	});
	//console.log(chat_room);
	//return chat_room.messages;  // return chat room message array
};

room_schema.statics.addMessage = function(message, room, cb) {
	// get message array for room
	this.findOne({name: room}, function(err, room) {
		
		// add new message to array
		room.messages.set(room.messages.length, message)    // arrays must be set this way so mongoose knows it needs to update document
	
		// update room in db
		room.save(function(err) {
			if (err)
				throw err;
			
			cb(null);
		});
	});
	
};

module.exports = mongoose.model('room', room_schema);