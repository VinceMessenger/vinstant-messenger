var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

var user_schema = new Schema({
	uid: { type: String, required: true, index: { unique: true } },
	password: { type: String, required: true },
	first_name: {type: String, required: true},
	last_name: {type: String, required: true},
	admin: {type: Boolean, required: true},
	rooms: {type: Array, required: true}
});

user_schema.pre('save', function(next) {
	var user = this;
	
	// if password not changed, just allow save
	if (!user.isModified('password'))
		return next();
		
	// generate a salt
	bcrypt.genSalt(10, function(err, salt) {
		if (err)
			throw err;   // error handling
			
		// hash password
		bcrypt.hash(user.password, salt, function(err, hash) {
			if (err)
				throw err;
			
			user.password = hash;  // override clear text password
			next();
		});
	});
});

user_schema.methods.comparePassword = function(toCheck, cb) {
	bcrypt.compare(toCheck, this.password, function(err, matched) {
		if (err)
			throw err;
			
	  cb(null, matched);
	});
};

module.exports = mongoose.model('user', user_schema);