var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var mongoose = require('mongoose');
var assert = require('assert');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var strip_tags = require('striptags');

var config = require('./config/config');
var User = require('./models/user_model');
var Message = require('./models/message_model');
var Room = require('./models/room_model');

var secret = config.secret;   // secret key for tokens

var users = {}; // this will be a hash table for socket id and user profile data
var room_clients = {};  // hash table for user arrays

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/bootstrap', express.static(__dirname + '/bootstrap-3.3.6-dist/'));
app.use('/js', express.static(__dirname + '/js/'));
app.use('/node_modules', express.static(__dirname + '/node_modules/'));
app.use('/views', express.static(__dirname + '/views/'));
app.use('/pictures', express.static(__dirname + '/pictures'));
app.use('/public', express.static(__dirname + '/public'));

// new user routing
app.get('/create_user', function(req, res) {
	res.sendFile(__dirname + "/login/new_user.html");
});

// create user
app.post('/create_user', function(req, res) {
	
	var new_user = new User({
		uid: strip_tags(req.body.uid),
		password: strip_tags(req.body.password),
		first_name: strip_tags(req.body.first_name),
		last_name: strip_tags(req.body.last_name),
		admin: false,
		rooms: ["General", "Games"]   // assign these two rooms to start
	});
	
	// check to see if uid is taken
	User.findOne({uid: new_user.uid}, function(err, user) {
		
		if (err)
			res.json({
				success: false,
				message: "User ID is already taken.",
				user: false,
				token: false
			});
			
		// fail login if uid exists
		if (user) {
			res.json({
				success: false,
				message: "User ID is already taken.",
				user: false,
				token: false
			});
		}
		else {
			// try saving user to db
			new_user.save(function(err) {
				if (err)
					throw err;
				
				// create and sign token if save successul
				var token = jwt.sign(new_user, secret, {
		       expiresIn: "12h" // expires in 24 hours
		    });
		    
		    // remove password from new_user object before sending it back
		    new_user.password = "";
		    
		    res.json({
		    	success: true,
		    	message: "New account has been created.",
		    	user: new_user,
		    	token: token
		    });
			});
		}
		
	});
});

// login routing
app.post('/login', function(req, res) {
	User.findOne({uid: req.body.uid}, function(err, user) {
		if (err)
			throw err;
			
		// fail login if uid doensn't exist
		if (!user) {
			res.json({
				success: false,
				message: "User ID or password is incorrect.",
				user: false,
				token: false
			});
		}
		else {
			user.comparePassword(req.body.password, function(err, matched) {
				
				if (matched) {
					//remove password from user object before sending it back
					user.password = "";
					
					res.json({
						success: true,
						message: "Successfully logged in.",
						user: user,
						token: jwt.sign(user, secret, {expiresIn: "12h" }) // expires in 24 hours
					});
				}
				else {
					res.json({
						success: false,
						message: "User ID or password is incorrect.",
						token: false
					});
				}
			});
		}
	});
});

app.post('/validate_token', function(req, res) {
	if(!req.body.token)
		return;
	
	jwt.verify(req.body.token, secret, function(err, decoded) {
		if (err) {
			if (err.name == "TokenExpiredError") {
				res.json({
					success: false,
					message: "Token Expired",
					token: false
				});
				
				console.log("token expired");
				return;
			}
			else
				throw err;
		}
			
		if (decoded._doc) {
			// remove password from user object
			decoded._doc.password = "";
			
			res.json({
				success: true,
				message: "Token authenticated successfully.",
				user: decoded._doc,
				token: req.body.token
				//token: jwt.sign(decoded._doc, secret, {expiresIn: "12h" }) // expires in 24 hours
			});
		}
		else {
			console.log("failed?");
			res.json({
				success: false,
				message: "Authentication failed.",
				token: false
			});
		}
	});
});

// login validation middleware
app.use(function(req, res, next) {
	var token = req.body.token;
	if (!token) {
		res.sendFile(__dirname + "/index.html");
	}
	else {
		validate_token(token, function(err, decoded){
			if(err)
				return;
			
			if(!decoded)
				return
				
			// pass middleware if token validates
			next();
		});
	}
});

app.use('/protected/views', express.static(__dirname + '/protected/views/'));

app.get('/', function (req, res) {
	 console.log("Got a GET request for the homepage");
   res.sendFile(__dirname + "/index.html");
})

// get messages protected service
app.post('/getMessages', function(req, res) {
	// todo //
	// make sure user has access to room
	// otherwise return nothing
	
	Room.findOne({name: req.body.room}, function(err, room){
		if(err)
			throw err;
		
		res.json({
    	success: true,
    	messages: room.messages
    });
    
	});	
});

// start server
server.listen(8081, '134.114.32.24', function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log("Server listening at http://%s:%s", host, port);

})

// connect mongo database
var mongo_url = config.database;
mongoose.connect(mongo_url, function(err) {
  assert.equal(null, err);
  console.log("Connected correctly to database.");
});

///// function to validate token //////
var validate_token = function(token, cb) {
	jwt.verify(token, secret, function(err, decoded) {
		 if (!decoded)
		    cb(err, null);
		 else
		    cb(err, decoded._doc);
	});
};

// socket.io stuffs
io.on('connection', function (socket) {
	
	socket.on('user_connected', function(user_obj) {
		// add socket to specified room
		socket.join(user_obj.room);
		
		// get user out of user_obj
		var user = user_obj.user;
		
		// add user to users hash table
		users[this.id] = user;
		
		var user_string = user.first_name + " " + user.last_name;
		
		console.log(user_string + " has logged in.");
		
		socket.broadcast.emit('user_connected', {user_string:user_string, socket_id: this.id});
		socket.to(user_obj.room).emit('joined_room', {user: user_string, socket_id: this.id});
	});
	
	socket.on('disconnect', function(socket) {
		
		if(users[this.id]) {
			var user_string = users[this.id].first_name + " " + users[this.id].last_name;
			
			// remove user from users hash table
			delete users[this.id];
			
			//console.log(user_string + " has logged out.");
			io.emit('user_disconnected', user_string);
		}
	});
  
  socket.on('new_message', function (message) {
  	message.uid = strip_tags(message.uid);
  	message.message = strip_tags(message.message); // strip out any html tags from message
  	message.room = strip_tags(message.room);
  	message.token = strip_tags(message.token);
  	
  	// make sure token user matches message user
  	validate_token(message.token, function(err, decoded) {
  		if(err)
  			return;
  		
			if(decoded.uid != message.uid)
				return;
		
			// create message object
			var new_message = new Message({
				sent_by: decoded.first_name + " " + decoded.last_name,
				message: message.message,
				uid: decoded.uid
			});
			
			// add admin tag if user is an admin
			if (decoded.admin)
				new_message.sent_by += " (Admin)";
			
			//// todo ///////
			// make sure chat room exists and user has access to it
			// otherwise return nothing
		
			// call model function to add message
			Room.addMessage(new_message, message.room, function(err) {
				if(err)
					throw err;
				
				// send saved message to clients		
				io.to(message.room).emit('chat_message', new_message);
			});
  	});
		
	});
	
	////// logic to change rooms ///////
	socket.on('changed_room', function (data) {
		socket.leave(data.old_room);
		socket.join(data.new_room);
		
		// send out message to old room that user has left
		var user_string = users[this.id].first_name + " " + users[this.id].last_name;
		socket.to(data.old_room).emit('left_room', user_string);
		
		// send out message to room that new user has joined
		socket.to(data.new_room).emit('joined_room', {user: user_string, socket_id: this.id});
		
	});
	
	////// let users know who is in room ///////
	socket.on('is_in_room', function (socket_id) {
		
		// send out message to room that user is in room
		var user_string = users[this.id].first_name + " " + users[this.id].last_name;
		socket.broadcast.to(socket_id).emit('is_in_room', user_string);
		
	});
	
	////// let new user know who is logged in ///////
	socket.on('is_logged_in', function (socket_id) {
		
		// send out message to room that user is logged in
		if(users[this.id]) {
			var user_string = users[this.id].first_name + " " + users[this.id].last_name;
			socket.broadcast.to(socket_id).emit('is_logged_in', user_string);
		}
		
	});

});
