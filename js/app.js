var app = angular.module('VinstantMessenger', ['ngRoute', 'ngCookies']);

app.config(function($routeProvider) {
  $routeProvider
  .when('/login', {
    controller: 'LoginController',
    templateUrl: 'views/login.html'
  })
  
  .when('/create_user', {
    controller: 'LoginController',
    templateUrl: 'views/new_user.html'
  })
  
  .when('/chat', {
    controller: 'ChatController',
    templateUrl: '/views/chat.html'
  })
  
  .otherwise({
    redirectTo: '/login'
  });
});
