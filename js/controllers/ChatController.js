app.controller('ChatController', ['$scope', '$location', '$route', 'user', 'messages', function($scope, $location, $route, user, messages) {
	
	var chat_div = document.getElementById("messages");
	var socket = io;
	$scope.message = {};
	
	////////////// chat room functionality /////////////////
	$scope.current_room = "General";  // hard code general as default for now
	$scope.rooms = user.getRooms();   // load in user's available rooms into array
	
	messages.setPopulated(false);
	
	// if user is not logged in, reroute them
	if(!user.loggedIn()) {
		$location.path('/login');
		$route.reload();
	}
	
	////////////////////// create socket events ///////////////////////////////
	
	else {
		// only connect socket when user is logged in
		socket = io.connect();
		
		socket.emit('user_connected', {"user": user.getUser(), "room": $scope.current_room});
		
		// socket events
		socket.on('chat_message', function(data) {
			var message_html;
			var divElement = angular.element(document.querySelector('#messages'));
			
			// append new message to chat div		
			if(data.uid == user.getUser().uid) {
   			message_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-right'><div class='list-group'><div class='list-group-item' style='background-color: #66CCFF'><h4 class='list-group-item-heading'>" + data.sent_by + "</h4><p class='list-group-item-text'>" + data.message + "</p></div></div></div></div>";
			}
			else {
				message_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-left'><div class='list-group'><div class='list-group-item' style='background-color: #99FF99'><h4 class='list-group-item-heading'>" + data.sent_by + "</h4><p class='list-group-item-text'>" + data.message + "</p></div></div></div></div>";
			}
			
			divElement.append(message_html);
			
			chat_div = document.getElementById("messages");
			chat_div.scrollTop = chat_div.scrollHeight;   // scroll div to bottom
		});
		
		// user connected
		socket.on('user_connected', function(data) {
			var html = "<p align='center' style='color:grey'>" + data.user_string + " has logged in.</p>";
			var divElement = angular.element(document.querySelector('#messages'));
			
			// add new paragraph to message div
			divElement.append(html);
			
			// scroll div to bottom
			chat_div = document.getElementById("messages");
			chat_div.scrollTop = chat_div.scrollHeight;   // scroll div to bottom
			
			// emit message to let new user know this user is in chat room as well
			socket.emit("is_logged_in", data.socket_id);
		});
			
		// user disconnected
		socket.on('user_disconnected', function(user_string) {
			var html = "<p align='center' style='color:grey'>" + user_string + " has logged out.</p>";
			var divElement = angular.element(document.querySelector('#messages'));
			
			// add new paragraph to message div
			divElement.append(html);
			
			// scroll div to bottom
			chat_div = document.getElementById("messages");
			chat_div.scrollTop = chat_div.scrollHeight;   // scroll div to bottom
				
		});
		
		socket.on('joined_room', function(data) {
			var html = "<p align='center' style='color:grey'>" + data.user + " has entered the chat room.</p>";
			var divElement = angular.element(document.querySelector('#messages'));
			
			// add new paragraph to message div
			divElement.append(html);
			
			// scroll div to bottom
			chat_div = document.getElementById("messages");
			chat_div.scrollTop = chat_div.scrollHeight;   // scroll div to bottom
			
			// emit message to let new user know this user is in chat room as well
			socket.emit("is_in_room", data.socket_id);
		});
		
		socket.on('left_room', function(user_string) {
			var html = "<p align='center' style='color:grey'>" + user_string + " has left the chat room.</p>";
			var divElement = angular.element(document.querySelector('#messages'));
			
			// add new paragraph to message div
			divElement.append(html);
			
			// scroll div to bottom
			chat_div = document.getElementById("messages");
			chat_div.scrollTop = chat_div.scrollHeight;   // scroll div to bottom
		});
		
		socket.on('is_in_room', function(user_string) {
			var html = "<p align='center' style='color:grey'>" + user_string + " is in this chat room.</p>";
			var divElement = angular.element(document.querySelector('#messages'));
			
			// add new paragraph to message div
			divElement.append(html);
			
			// scroll div to bottom
			chat_div = document.getElementById("messages");
			chat_div.scrollTop = chat_div.scrollHeight;   // scroll div to bottom
		});
		
		socket.on('is_logged_in', function(user_string) {
			var html = "<p align='center' style='color:grey'>" + user_string + " is logged in.</p>";
			var divElement = angular.element(document.querySelector('#messages'));
			
			// add new paragraph to message div
			divElement.append(html);
			
			// scroll div to bottom
			chat_div = document.getElementById("messages");
			chat_div.scrollTop = chat_div.scrollHeight;   // scroll div to bottom
		});
		
	}
	//////////////////////// end socket events ////////////////////////////////////////////////////
	
	
	
	// get messages from messages service for home chat room
	//messages.fetchMessages({token: user.getToken()}, function(){
	messages.fetchMessages({token: user.getToken(), room: $scope.current_room}, function(){
		$scope.messages = messages.getMessages();
		$scope.populate_messages();   // put retrieved messages into chat div
		chat_div.scrollTop = chat_div.scrollHeight;
	});
	
	$scope.user = user.getUser();
	$scope.token = user.getToken();
	
	$scope.logout = function() {
		user.logout();
		$location.path('/login');
		$route.reload();
	};
  
  /////////////// send messages /////////////////
  $scope.sendMessage = function() {
  	if(!$scope.message.message)
  		return;
  	
  	// add sender to message
  	$scope.message.uid = user.getUser().uid;
  	
  	// add current room to message
  	$scope.message.room = $scope.current_room;
  	$scope.message.token = user.getToken()
  	
  	socket.emit('new_message', $scope.message);
  	
  	// clear out message holder
  	$scope.message = {};
  };
	
	////////// handle $destroy /////////////
	$scope.$on('$destroy', function() {
		try {
			socket.disconnect();
		}
		catch (err) {
			return
		}
	});
	
	
	///////////// function to populate the message div ////////////
	$scope.populate_messages = function() {
		if (!messages.checkPopulated()) {
			
			var message_html;
			var divElement = angular.element(document.querySelector('#messages'));
			
			// first we need to clear the contents of the chat div
			divElement.empty();
			
			divElement.innerHtml = "";
			
			if ($scope.messages) {
				for(var i = 0; i < $scope.messages.length; i++) {
					// check if user sent this message
					if($scope.messages[i].uid == user.getUser().uid) {
						message_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-right'><div class='list-group'><div class='list-group-item' style='background-color: #66CCFF'><h4 class='list-group-item-heading'>" + $scope.messages[i].sent_by + "</h4><p class='list-group-item-text'>" + $scope.messages[i].message + "</p></div></div></div></div>";
					}
					else {
						message_html = "<div class='row'><div class='col-lg-8 col-xs-8 pull-left'><div class='list-group'><div class='list-group-item' style='background-color: #99FF99'><h4 class='list-group-item-heading'>" + $scope.messages[i].sent_by + "</h4><p class='list-group-item-text'>" + $scope.messages[i].message + "</p></div></div></div></div>";
					}
					
					// add message to chat area
					divElement.append(message_html);
				}
			}
			
			messages.setPopulated(true);  // set populated flag so the div won't populate again
		}
	};
	
	///////////// function to change current room /////////////
	$scope.change_room = function(new_room) {
		var old_room = $scope.current_room;  // get old room so we can remove socket
		$scope.current_room = new_room;
		
		if(old_room != new_room) {
		
			// fetch messages for new room
			messages.fetchMessages({token: user.getToken(), room: $scope.current_room}, function(){
	
				$scope.messages = messages.getMessages();
				$scope.populate_messages();   // put retrieved messages into chat div
				chat_div.scrollTop = chat_div.scrollHeight;
				
				// remove socket from current room, add to new room
				socket.emit('changed_room', {"user": user.getUser(), "new_room": $scope.current_room, "old_room": old_room});
			});
		}
	};
	
	chat_div = document.getElementById("messages");
	
}]);