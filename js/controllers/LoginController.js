app.controller('LoginController', ['$scope', '$http', '$location', '$route', 'user', function($scope, $http, $location, $route, user) {
	
	// regular expressions
	$scope.name_regex = new RegExp("^[A-Z][a-z]{2,20}$");
	$scope.uid_regex = new RegExp("^[a-z]{2,10}[0-9]{1,5}$");
	$scope.pass_regex = new RegExp("^[A-Za-z0-9!@#$%^&*()_]{6,}$");
	
	$scope.error = "";  // holder for error messages
	
	if(user.loggedIn()) {
		$location.path('/chat');
		$route.reload();
	}
	
	// check for token in cookies
	user.validateToken()
		.then(function() {
			if(user.loggedIn()) {
				$location.path('/chat');
				$route.reload();
			}
	});
	
	$scope.form_data = {};
	$scope.user = {};
	
	// login service
	$scope.login = function() {
		
		// make sure uid and password have values
		if(!$scope.form_data.uid || !$scope.form_data.password)
			return;
		
		user.login($scope.form_data)
		.then(function() {
			if(user.loggedIn()) {
				$location.path('/chat');
				$route.reload();
			}
			else {
				document.getElementById("login_error").style.visibility = "visible";
		  	document.getElementById("uid").select();
			}
   });
	};
	
	// account creation service
	$scope.create_user = function() {
		
		// make sure we have valid values
		// first name
		if(!$scope.name_regex.test($scope.form_data.first_name)){
			$scope.error = "First Name is invalid.";
			document.getElementById("first_name").select();
			document.getElementById("create_error").style.visibility = "visible";
			return;
		}
		else {
			document.getElementById("create_error").style.visibility = "hidden";
		}
		
		// last name
		if(!$scope.name_regex.test($scope.form_data.last_name)){
			$scope.error = "Last Name is invalid.";
			document.getElementById("last_name").select();
			document.getElementById("create_error").style.visibility = "visible"; 
			return;
		}
		else {
			document.getElementById("create_error").style.visibility = "hidden";
		}
		
		// uid
		if(!$scope.uid_regex.test($scope.form_data.uid)){
			$scope.error = "User ID is invalid. Must be lowercase letters followed by numbers.";
			document.getElementById("uid").select();
			document.getElementById("create_error").style.visibility = "visible";
			return;
		}
		else {
			document.getElementById("create_error").style.visibility = "hidden";
		}
		
		// password validation
		if(!$scope.pass_regex.test($scope.form_data.password)){
			$scope.error = "Password is invalid. Must be at least 6 characters and have no unreadable characters.";
			document.getElementById("password").select();
			document.getElementById("create_error").style.visibility = "visible";
			return;
		}
		else {
			document.getElementById("create_error").style.visibility = "hidden";
		}
		
		// try to create user
		var status = user.create_user($scope.form_data)
			.then(function() {
				if(user.loggedIn()) {
					$location.path('/chat');
					$route.reload();
				}
				else {
					$scope.error = "The User ID specified is already in use.";
					document.getElementById("create_error").style.visibility = "visible";
		  		document.getElementById("uid").select();
				}
			});
	};
	
}]);