app.directive('incoming-message', function() { 
  return { 
    restrict: 'E', 
    scope: { 
      message: '=' 
    }, 
    templateUrl: 'js/directives/incoming_message.html' 
  }; 
});