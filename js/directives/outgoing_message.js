app.directive('outgoingMessage', function() { 
  return { 
    restrict: 'E', 
    scope: { 
      message: '=' 
    }, 
    templateUrl: 'js/directives/outgoing_message.html' 
  }; 
});