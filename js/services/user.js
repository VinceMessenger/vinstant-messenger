app.factory('user', ['$http', '$location', '$route', '$cookies', function($http, $location, $route, $cookies) {
	
	var user = {};
	
	user.logged_in = false;
	user.token = "";
	user.rooms = [];  // array of user's rooms
	user.current_room; // hold the user's last visited room

	user.login = function(form_data) {
	    return $http.post('/login', form_data)
				.success(function(data) {
					if(data.success) {
						user.token =  data.token;
						user.user = data.user;
						user.logged_in = true;
						user.rooms = data.user.rooms;
						
						// set cookie value for token
						$cookies.remove("vm-token");
						$cookies.put("vm-token", data.token);
					}
				})
				.error(function(err) {
					return err;
				});
		};
					
		user.create_user = function(form_data) {
			return $http.post('/create_user', form_data)
				.success(function(data) {
					if (data.success) {
						user.token = data.token;
						user.user = data.user;
						user.logged_in = true;
						user.rooms = data.user.rooms;
						
						// set cookie value for token
						$cookies.remove("vm-token");
						$cookies.put("vm-token", data.token);
					}
					else {
						return false;
					}
				})
				.error(function(err) {
					return err;
				});
		};
		
		user.logout = function() {
			user.logged_in = false;
			user.token = false;
			user.user = false;
			
			// remove token cookie
			$cookies.remove("vm-token");
		};
					
		user.getToken = function() {
			return this.token;
		};
		
		user.getUser = function() {
			return this.user;
		};
		
		user.validateToken = function() {
			
			// check for token and try validating it	
			user.token = $cookies.get("vm-token");
			
			return $http.post('/validate_token', {"token": user.token})
				.success(function(data) {
					if (data.success) {
						user.user = data.user;
						user.token = data.token;
						user.logged_in = true;
						user.rooms = data.user.rooms;
						//$cookies.remove("vm-token");
						//$cookies.put("vm-token", data.token);
					}
				});
		};
		
		user.loggedIn = function() {
			return this.logged_in;
		};
		
		user.getRooms = function() {
			return this.rooms;
		};
				
	return user;
}]);