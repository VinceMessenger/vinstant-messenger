app.factory('messages', ['$http', function($http) {
	
	var messages = {};  // create object before adding methods
	messages.populated = false;
	
	messages.fetchMessages = function(data, cb) {
	    return $http.post('/getMessages', data)
				.success(function(data) {
					if(data.success) {
						messages.populated = false;
						messages.messages = data.messages;
					}
					
					cb();
				})
				.error(function(err) {
					return err;
				});
	};
	
	messages.getMessages = function() {
		return messages.messages;   // return array of messages to client
	};
	
	messages.setPopulated = function(flag) {
		messages.populated = flag;
	};
	
	messages.checkPopulated = function() {
		return messages.populated;
	};
	
	return messages;
	
}]);